FROM ubuntu:18.04

MAINTAINER Jawira Portugal

RUN apt-get update && apt-get install -y \
    apt-utils \
    make \
    wget \
    && apt-get autoremove -y \
    && apt-get autoclean \
    && apt-get clean \
    && rm -rf /tmp/* /var/tmp/* /var/lib/apt/lists/*
