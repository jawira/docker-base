docker-base
===========

This image is based on Ubuntu Bionic and includes:

- make
- rsync
- wget

Links:

- Bitbucket: https://bitbucket.org/jawira/docker-base/
- Docker Hub: https://hub.docker.com/r/jawira/base/
